# Technical Part-1
# CYPRESS-Contact-Integarion tests
Integration testing using cypress intercept

## Requirements
- git
- Javascript
- npm
- cypress

## Instructions for OsX to run local

## How to run front end 

- move inside app folder and run below command 
### `npm start`

- make sure contact manager app is running on (http://localhost:3000)

- Clone the otrium repo and move in to repo folder 
  `cd OTRIUM`

- Run the following commands to execute the tests
  `npm install`
  `npx cypress open`

- Run contact tests under integration_tests folder

# Technical Part-2
# CYPRESS-E2E-tests
E2E testing using cypress

## Requirements
- git
- Javascript
- npm
- cypress

## Instructions for OsX to run local

## How to run E2e tests
- Clone the otrium repo and move in to repo folder 
  `cd OTRIUM`
- set otrium userid and name in testdata.json file
- Run the following commands to execute the tests
  `npm install`
  `npx cypress open`

- Run place_order tests under e2e_tests folder


# Technical Part-3
# CYPRESS-Medium-RestAPI
This is project for rest api test automation using cypress

## Requirements
- git
- Javascript
- npm
- cypress

## Instructions for OsX to run local

- Clone the repo and move in to repo folder 
  `cd OTRIUM`
- Create your integrated token on medium app and set userid and name in mediumApiData.json file

- Run the following commands to execute the tests
  `npm install`
  `npx cypress open`

- Run medium_api tests under api_tests folder

## Instructions to  run on Gitlab
- Navigate to CI/CD and click on "Run Pipeline" 

## End Point documentation  used in framework 
- Getting the authenticated user’s details

  `GET https://api.medium.com/v1/me`

- Creates a post on the authenticated user’s profile.

  `POST https://api.medium.com/v1/users/{{authorId}}/posts`

- Returns a full list of publications that the user is related to in some way

  `GET https://api.medium.com/v1/users/{{userId}}/publications`

- For details please go through following link.
  
  [Medium API]

[Medium API]: https://github.com/Medium/medium-api-docs

### The project directory structure
The project follows the standard directory structure used in cypress projects:
otrium
  + cypress
  + integration   
    + api_tests                   medium_api    
    + e2e_tests                   place_order
    + integration_tests           contact
  + plugins                       index
  + screenshots
  + support
  + videos 
  + pages                         login
                                   otrium

## How to run tests on browserstack
- Clone the otrium repo and move in to repo folder 
  `cd OTRIUM`
- set browserstack username and access-key in browserstack.json file
- Run the following commands to execute the tests
  `npm install`
  - Install the CLI 
  `npm install -g browserstack-cypress-cli`
  - Configure tests
  `browserstack-cypress init`
  - Run tests
  `browserstack-cypress run --sync`
                                   