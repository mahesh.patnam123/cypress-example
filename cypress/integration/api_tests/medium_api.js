/// <reference types="cypress" />

const dataJson = require('../../fixtures/createPost.json');
const mediumApiJson = require('../../fixtures/mediumApiData.json');

describe('Medium API Automation',  ()=> {
  let user_id = '';

it('Test getUser information api with valid token',()=>{
  cy.request({
    method:'GET',
    url:Cypress.env("mediumUrl")+'/v1/me',
    headers:{
      'Authorization':'Bearer '+mediumApiJson.accessToken
    }
  }).then((response)=>{
    cy.log(JSON.stringify(response));
    user_id = response.body.data.id;
    expect(response.status).to.eq(200);
    expect(response.body.data.username).to.eq(mediumApiJson.mediumuserName);
    expect(response.body.data.name).to.eq(mediumApiJson.mediumName);
  })
})

it('Test getUser information api with invalid token',()=>{
  cy.request({
    method:'GET',
    url:Cypress.env("mediumUrl")+'/v1/me',
    headers:{
      'Authorization':'Bearer '+mediumApiJson.invalidAccessToken
    },
    failOnStatusCode:false
  }).then((response)=>{
    cy.log(JSON.stringify(response));;
    expect(response.status).to.eq(401);
    expect(response.body.errors[0].code).to.eq(6003);
    expect(response.body.errors[0].message).to.eq('Token was invalid.');
  })
})

it('Test create publications with valid User id',  ()=> {
  cy.request({
    method:'POST',
    url:Cypress.env("mediumUrl")+'/v1/users/'+user_id+'/posts',
    headers:{
      'Authorization':'Bearer '+mediumApiJson.accessToken,
      'Content-Type':'application/json'
    },
    body:{
      "title":dataJson.title,
      "contentFormat":dataJson.contentFormat,
      "content":dataJson.content,
      "canonicalUrl":dataJson.canonicalUrl,
      "tags":dataJson.tags,
      "publishStatus":dataJson.publishStatus
    }
  }).then(function(response){
    cy.log(JSON.stringify(response));
    expect(response.status).to.eq(201);
    expect(response.body.data.authorId).to.eq(user_id);
  })
})

it('Test create publications with invalid User id',  ()=> {
  cy.request({
    method:'POST',
    url:Cypress.env("mediumUrl")+'/v1/users/'+mediumApiJson.invalidUserId+'/posts',
    headers:{
      'Authorization':'Bearer '+mediumApiJson.accessToken,
      'Content-Type':'application/json'
    },
    body:{
      "title":dataJson.title,
      "contentFormat":dataJson.contentFormat,
      "content":dataJson.content,
      "canonicalUrl":dataJson.canonicalUrl,
      "tags":dataJson.tags,
      "publishStatus":dataJson.publishStatus
    },
    failOnStatusCode:false
  }).then((response)=>{
    cy.log(JSON.stringify(response));
    expect(response.status).to.equal(400);
    expect(response.body.errors[0].code).to.eq(6026);
  })
})

  it('Test get User publications with valid userid',  ()=> {
        cy.request({
          method:'GET',
          url:Cypress.env("mediumUrl")+'/v1/users/'+user_id+'/publications',
          headers:{
            'Authorization':'Bearer '+mediumApiJson.accessToken
          }
        }).then((response)=>{
          cy.log(JSON.stringify(response));
          expect(response.status).to.equal(200);
        })
      })

      it('Test get User publications with invalid userid',  ()=> {
        cy.request({
          method:'GET',
          url:Cypress.env("mediumUrl")+'/v1/users/'+mediumApiJson.invalidUserId+'/publications',
          headers:{
            'Authorization':'Bearer '+mediumApiJson.accessToken
          },
          failOnStatusCode:false
        }).then((response)=>{
          cy.log(JSON.stringify(response));
          expect(response.status).to.equal(400);
        })
      })
    })