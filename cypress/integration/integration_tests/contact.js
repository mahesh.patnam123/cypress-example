/// <reference types="cypress" />

describe('Contacts manager app integration tests', () => {
  
  
 it('Contact manager app integration tests',()=>{
    cy.intercept('GET','/contacts',{fixture:'createContact.json'}).as('get');
    cy.visit(Cypress.env("contactManagerUrl"));
    cy.contains('Contact Manager').should('be.visible');
    cy.wait('@get').then(inter=>{
    cy.log(JSON.stringify(inter));
    });
  })
})