// type definitions for Cypress object "cy"
/// <reference types="cypress" />
const {
  userLogin
} = require("../../../pages/login");

const {
  searchBrand,
  addProduct,
} = require("../../../pages/product");

const {
  addToCart,
  checkout,
} = require("../../../pages/cart");

describe('Otrium E2E Automation Tests', function () {
  //Use the cy.fixture() method to pull data from fixture file
  before(function () {
    //Provide the data from the fixture
     cy.getData('testdata');
  })
  beforeEach(() => {
    //Launches otrium Url
    cy.visit('/');
  })
  it('Add adidas product', function () {
    userLogin(this.data.userId,this.data.password);
    searchBrand(this.data.brandName);
    addProduct(this.data.productName);
    addToCart();
    checkout(this.data.firstName,this.data.lastName,this.data.phoneNumber,this.data.postalCode,this.data.houseNumber);
  })
})
 
