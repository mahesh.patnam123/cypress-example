const cartSelector = '[data-testid=main-header-my-account-button] > svg';
const addCartSelector = '[data-testid=main-header-cart-a]';
const continueToCheckoutSelector = '.cart-page-title > .wc-proceed-to-checkout > .proceed-to-checkout';
const firstNameSelector = '#shipping_first_name';
const laststNameSelector = '#shipping_last_name';
const phoneSelector = '#shipping_phone';
const postCodeSelector = '#shipping_postcode';
const houseNumberSelector = '#shipping_house_number';
const shipAddressSelector = '#ship-to-different-address-checkbox';
const continueToPaySelector = '#step3 > .checkout-btn-cnt-new';

/**
 * Adds product to cart
 */
 function addToCart() {
cy.clickElement(cartSelector);
cy.clickElement(addCartSelector);
cy.url().should('include','cart');
cy.clickElement(continueToCheckoutSelector);
}

/**
 * Does checkout 
 * @param {String} firstName 
 * @param {String} lastName 
 * @param {String} phoneNumber 
 * @param {String} postalCode 
 * @param {String} houseNumber 
 */
 function checkout(firstName,lastName,phoneNumber,postalCode,houseNumber) {
  cy.url().should('include','checkout');
  cy.enterText(firstNameSelector,firstName);
  cy.enterText(laststNameSelector,lastName);
  cy.enterText(phoneSelector,phoneNumber);
  cy.enterText(postCodeSelector,postalCode);
  cy.enterText(houseNumberSelector,houseNumber);
  cy.clickCheckbox(shipAddressSelector);
  cy.clickElement(continueToPaySelector);
  }

module.exports = {
  addToCart,
  checkout
};