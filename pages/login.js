
const signinButtonSelector = '.css-rluzqx';
const usernameSelector = '[data-testid="login-form-email-input"]';
const passwordSelector = '[data-testid="login-form-password-input"]';
const loginButtonSelector = '[data-testid="login-form-login-button"]';

/**
 * 
 * @param {String} usernameValue 
 * @param {String} passwordValue 
 */
  function userLogin(usernameValue, passwordValue) {
  cy.clickElement(signinButtonSelector);
  cy.enterText(usernameSelector, usernameValue);
  cy.enterText(passwordSelector, passwordValue);
  cy.clickElement(loginButtonSelector);
}
module.exports = {
  userLogin
};