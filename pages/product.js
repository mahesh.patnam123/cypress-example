
const searchButtonSelector = '[data-testid="main-header-search-btn"]';
const searchBoxSelector = '[data-testid="search-input"]';
const brandNameSelector = '//a[@href="/sales/women/adidas"]//div[@class="css-i9tdzg"]//div[@class="css-1chop8f"]//span[contains(text(),"Brand")]';
const selectProductSizeDropdownSelector = '[data-testid=product-order-select-size]';
const productSizeSelector = '[data-testid=product-order-select-size] > .visible > :nth-child(2)';
const addProductSelector = '[data-testid=product-order-button]';
const brandSelector = 'div.css-i9tdzg>div.css-kicnke';

/**
 * 
 * @param {String} brandName 
 */
  function searchBrand(brandName) {
    cy.clickElement(searchButtonSelector);
    cy.enterText(searchBoxSelector,brandName);
    cy.enterText(searchBoxSelector, '{enter}');
    cy.clickFirstElement(brandSelector);
    cy.url().should('include', brandName);
}

/**
 * 
 * @param {String} productName 
 */
 function addProduct(productName) {
  cy.clickTextElement(productName);
  cy.clickElement(selectProductSizeDropdownSelector);
  cy.clickElement(productSizeSelector);
  cy.clickElement(addProductSelector);
}


module.exports = {
  searchBrand,
  addProduct,
};